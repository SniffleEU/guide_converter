import json
import codecs
import enum


class LineType(enum.Enum):
    Accept_Quest = "Pick Up"  # Accept quest at giver
    Progress_Quest = "Progress Quest"  # Optional, don't complete, but do as you move
    Deliver_Quest = "Hand In"  # Deliver completed quest
    Complete_Quest = "Complete Quest"  # Complete the objectives of the quest
    Go = "Go"
    Grind = "Grind"
    Level_Up = "DING"
    Vendor = "Vendor"
    Buy = "Buy"
    Train = "Train"
    Hearthstone = "Hearth"
    Set_Hearth = "Set Hearth"
    Die = "Die"
    Spirit_Rez = "Spirit Rez"
    Get_Flight_Path = "Get Flight Path"
    Fly = "Fly"
    Vendor_Repair = "Vendor + Repair"
    Craft = "Craft"
    Loot = "Loot"
    Save = "Save"  # Don't Vendor X item
    Teleport = "Teleport"
    Tame = "Tame"
    Learn = "Learn"
    Abandon_Pet = "Abandon Pet"
    Beast_Training = "Beast Training"
    Feed_Pet = "Feed Pet"
    Use = "Use"
    Accept_Quest_Item = "Accept Item Quest"
    Progress_Objective = "Progress Objective"
    Complete_Objective = "Complete Objective"
    Discover = "Discover"
    Destroy = "Destroy"
    Bank_Deposit = "Bank Deposit"
    Bank_Withdrawal = "Bank Withdrawal"
    Skip_for_now = "Skip for now"
    Skip = "Skip"
    Hand_In = "Hand In*"
    Pick_Up = "Pick Up*"
    Accept_Quest_ItemAlt = "Accept Item Quest*"
    Dungeon = "Dungeon"
    Pick_Pocket = "Pick Pocket"
    Pick_Lock = "Pick Lock"
    Quick_Hand_In = "Quick Hand In"
    SkipAlt = "Skip*"
    Start_Find_Group = "Start Finding Group"
    Find_Group = "Find Group"
    LootAlt = "Loot*"
    Stable_Pet = "Stable Pet"
    Withdraw_Pet = "Withdraw Pet"
    Get = "Get"
    Blink = "Blink"
    Jump = "Jump"
    Repair = "Repair"
    Equip = "Equip"
    Complete_Dungeon = "Complete Dungeon"
    Cook = "Cook"
    Disenchant = "Disenchant"
    Cast = "Cast"


full_json = codecs.open('Alliance/12-60 Alliance clean.json', 'r', 'utf-8-sig')
json_object = json.load(full_json)


def classcheck(line):
    number = 3
    tempstr = ""
    if line[3] != "":
        tempstr += "[A "
        if "D" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Druid"
        if "S" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Shaman"
        if "W" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Warrior"
        if "H" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Hunter"
        if "M" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Mage"
        if "R" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Rogue"
        if "Pr" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Priest"
        if "l" in line[3] or "L" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Warlock"
        if "Pa" in line[3]:
            if len(tempstr) > number:
                tempstr += ", "
            tempstr += "Paladin"
        tempstr += "]"
    else:
        tempstr = ""

    return tempstr


for section in json_object:
    # SECTION = number section currently on NOT THE SECTION DATA
    # CREATE NEW FILE HERE
    # SPLIT BY SECTION
    f = open("Alliance " + section + ".lua", "w")
    f.write("Guidelime.registerGuide([[\n")
    f.write("[D21-23]\n")
    f.write("[GA Allince]\n")
    f.write("[N 21-23 Horde]\n")
    f.write("[NX21-23 Horde]\n")

    for step in json_object[section]:
        # STEP = number step currently on NOT THE STEP DATA
        for line in json_object[section][step]:
            # LINE in the step
            # Each "line" = line array
            guidelimestr = ""
            if line[1] == LineType.Accept_Quest.value:
                guidelimestr = "Accept [QA" + line[6] + "] from " + line[9]

            elif line[1] == LineType.Progress_Quest.value:
                guidelimestr = "Progress [QC" + line[6] + "][L " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] [O]"

            elif line[1] == LineType.Complete_Quest.value:
                if line[12] != "":
                    guidelimestr = "Complete [QC" + line[6] + "][L " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"
                else:
                    guidelimestr = "Complete [QC" + line[6] + "]"

            elif line[1] == LineType.Deliver_Quest.value:
                guidelimestr = "Turn in [QT" + line[6] + "] to " + line[9] + " " + line[14]

            elif line[1] == LineType.Go.value:
                if line[12] != "":
                    guidelimestr = "Go [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] " + line[8]
                else:
                    guidelimestr = "Go " + line[13].replace('-', ' ') + " " + line[8]

            elif line[1] == LineType.Grind.value:
                # ITEM, REP PREFIX!
                x = line[8].split(' ')
                if x[0] == "to":
                    if len(x) > 2:
                        y = int(x[3]) - int(x[1])
                        guidelimestr = "Grind to [XP" + str((int(x[4][1:]) + 1)) + "-" + str(y) + " " + str(x[1]) + " / " + str(x[3]) + "]"
                    else:
                        guidelimestr = "Grind to [XP" + line[8][3:] + "]"
                elif x[0] == "REP":
                    guidelimestr = "Grind rep" + line[8][3:]
                elif x[0] == "ITEM":
                    guidelimestr = "Grind " + line[9] + " For " + line[8][4:] + " at [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif LineType.Level_Up.value in line[1]:
                x = line[1].split(' ')
                guidelimestr = "You should be lvl [XP" + x[1] + "]"

            elif line[1] == LineType.Vendor.value:
                guidelimestr = "Visit " + line[9] + " at [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] to [V] Vendor"

            elif line[1] == LineType.Buy.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = "Buy " + line[7] + " " + item + " from " + line[9] + " at [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Train.value:
                if line[8] == "":
                    guidelimestr = "Visit " + line[9] + " at [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] to Train[T]"

                else:
                    item = line[8]
                    item = item.replace("[", "")
                    item = item.replace("]", "")
                    guidelimestr = "Visit " + line[9] + " at [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] to Train " + item

            elif line[1] == LineType.Hearthstone.value:
                guidelimestr = "Hearth to [H" + line[8][2:] + "]"

            elif line[1] == LineType.Set_Hearth.value:
                guidelimestr = "Set hearth in [S" + line[8][2:] + "]"

            elif line[1] == LineType.Die.value:
                if line[12] != "":
                    guidelimestr = "Die " + " [L " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] " + line[8]
                else:
                    guidelimestr = "Die"

            elif line[1] == LineType.Spirit_Rez.value:
                if line[12] != "":
                    guidelimestr = "Spirit res" + " [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] " + line[8]
                else:
                    guidelimestr = "Spirit res " + line[8]

            elif line[1] == LineType.Get_Flight_Path.value:
                guidelimestr = line[1] + " " + line[8][:3] + "[P " + line[8][3:] + "]"

            elif line[1] == LineType.Fly.value:
                guidelimestr = "Fly to [F " + line[8][3:] + "]"

            elif line[1] == LineType.Vendor_Repair.value:
                guidelimestr = "Visit " + line[9] + " at [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "] to [V] Vendor & [R] repair"

            elif line[1] == LineType.Craft.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = "Craft " + item

            elif line[1] == LineType.Loot.value or line[1] == LineType.LootAlt.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                if line[12] != "":
                    guidelimestr = "Loot " + item + "From " + line[9] + " [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"
                else:
                    guidelimestr = "Loot " + item

            elif line[1] == LineType.Save.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = "Save " + " " + item

            elif line[1] == LineType.Teleport.value:
                guidelimestr = "Teleport to " + line[8]

            elif line[1] == LineType.Tame.value:
                guidelimestr = "Tame " + line[9] + " Around [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Learn.value:
                guidelimestr = "Learn " + line[8]

            elif line[1] == LineType.Abandon_Pet.value:
                guidelimestr = "Abandon pet"

            elif line[1] == LineType.Feed_Pet.value:
                guidelimestr = "Feed pet to happy"

            elif line[1] == LineType.Beast_Training.value:
                guidelimestr = line[1] + " " + line[8]

            elif line[1] == LineType.Use.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                if line[12] == "":
                    guidelimestr = line[1] + " " + item
                else:
                    guidelimestr = line[1] + " " + item + " " + line[9] + " [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Accept_Quest_Item.value:
                item = line[9]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = line[1] + " " + item

            elif line[1] == LineType.Progress_Objective.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = line[1] + " " + line[8] + " " + item + " [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "][O]"

            elif line[1] == LineType.Complete_Objective.value:
                guidelimestr = line[1] + " " + line[8] + " " + line[9] + " [L " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Discover.value:
                guidelimestr = line[1] + " " + line[8] + " [G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Destroy.value:
                if line[9] == "":
                    item = line[8]
                else:
                    item = line[9]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = line[1] + " " + item

            elif line[1] == LineType.Bank_Deposit.value:
                guidelimestr = line[1] + " " + line[8] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Bank_Withdrawal.value:
                guidelimestr = line[1] + " " + line[8] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Pick_Up.value:
                guidelimestr = "Accept [QA" + line[6] + "]"

            elif line[1] == LineType.Hand_In.value:
                guidelimestr = "Complete [QC" + line[6] + "]"

            elif line[1] == LineType.Skip.value or line[1] == LineType.Skip_for_now.value or line[1] == LineType.SkipAlt.value:
                guidelimestr = "Skip [QS" + line[6] + "]"

            elif line[1] == LineType.Dungeon.value:
                guidelimestr = "Do the dungeon " + line[8] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Pick_Lock.value:
                guidelimestr = "Pick lock " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Pick_Pocket.value:
                guidelimestr = "Pick pocket " + line[8] + " from " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Quick_Hand_In.value:
                if line[6] != "":
                    guidelimestr = "Hand in [QC" + line[6] + "] To " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"
                else:
                    guidelimestr = "Hand in " + line[7] + " To " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Accept_Quest_ItemAlt.value:
                item = line[9]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = line[1] + " " + item

            elif line[1] == LineType.Find_Group.value or line[1] == LineType.Start_Find_Group.value:
                guidelimestr = "Find or start finding group " + line[8]

            elif line[1] == LineType.Find_Group.value or line[1] == LineType.Start_Find_Group.value:
                guidelimestr = "Find or start finding group " + line[8]

            elif line[1] == LineType.Stable_Pet.value:
                guidelimestr = line[1] + " at " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Withdraw_Pet.value:
                guidelimestr = line[1] + " at " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Get.value:
                guidelimestr = line[1] + " " + line[8] + " " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Blink.value:
                guidelimestr = line[1] + " " + line[8] + " " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Jump.value:
                guidelimestr = line[1] + " " + line[8] + " " + line[9] + "[G " + line[12].replace("~", "") + " " + line[13].replace('-', ' ') + "]"

            elif line[1] == LineType.Repair.value:
                guidelimestr = "Visit " + line[9] + " at [G " + line[12] + " " + line[13].replace('-', ' ') + "] to [R] Repair"

            elif line[1] == LineType.Equip.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = line[1] + " " + item

            elif line[1] == LineType.Complete_Dungeon.value:
                guidelimestr = "Complete " + line[8]

            elif line[1] == LineType.Cook.value:
                item = line[9]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = line[1] + " " + item

            elif line[1] == LineType.Disenchant.value:
                item = line[8]
                item = item.replace("[", "")
                item = item.replace("]", "")
                guidelimestr = line[1] + " " + item

            elif line[1] == LineType.Cast.value:
                guidelimestr = line[1] + " " + line[8]

            else:
                print("CHECKHERE: " + str(line))

            guidelimestr += str(classcheck(line))
            if line[16] != "":
                note = line[16]
                note = note.replace("[", "")
                note = note.replace("]", "")
                guidelimestr += " \\\\" + note

            if guidelimestr != "":
                f.write(guidelimestr + "\n")
    f.write("]], 'Guidelime_Sniffle')")
